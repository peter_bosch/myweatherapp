#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { ServerlessWeatherCdkStack } from '../lib/serverless_weather_cdk-stack';
require('dotenv').config();

const app = new cdk.App();
new ServerlessWeatherCdkStack(app, 'ServerlessWeatherCdkStack', {
  env: { 
    // use environment variables pass the environment details to your application
    account: process.env.CDK_DEFAULT_ACCOUNT, 
    region: process.env.CDK_DEFAULT_REGION 
  }
});
