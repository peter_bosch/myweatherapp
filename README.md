# Welcome to the setup of a weather app

## This was the given technical assessment:

Create a RESTful API (built with node.js with MongoDB Atlas as DB).
 
Please provide with the following URLs, PrintScreen and source code:
 
URLs
-  /currenttempincovilha

   You will need to provide the current temperature in Covilha, Portugal and the respective timestamp of the reading (not from the browser, but from the server).
-  /avgtempinsfax

   Temperature Average in Sfax, Tunisia in June
 
PrintScreen
- MongoDB Collection and respective documents.
 
SourceCode:
- You are the one if you are able to do it using AWS Services like AWS Lambda + API Gateway + Cloud Formation and with a testing pipeline.
- Gitlab or Github repo with the Nodejs code and cloud formation code (if you use AWS Services)
 

## Assumptions made
As the assessment is talking about a setup with mongoDB, and a timestamp of the reading, this is interpreted as follows:
- We have a database that can be frequently updated with the temperature of a city, inclusive the timestamp of the reading of the temperature. This data is stored in the collection 'live_temp'. The collection is part of this solution, filling the database on a frequent basis not. The table is filled with an init script. 
- To store historical data, the collection 'hist_temp' is created. For a certain city per day, 3 temperatures are stored in this collection: min, max and average. Here too, the collection is part of the solution, filling the database is not. One could fill this collection based on the 'live_temp' collection on a daily basis.  


## Setup

### Prerequisites:
- Setup a mongoDB Atlas (M0 cluster will do). 
- Clone the repo 
- The repo has two parts: 
  - app: this is for the node.js app, which can run locally for development and testing
  - infra: for creating the AWS services, using CDK on a infra as code WoW. 
- cd app
- add a .env file in the app, which holds 'DB_CONNECTION=mongodb+srv://`<user>`:`<password>`@`<cluster>`.mongodb.net/retryWrites=true&w=majority'
- run `npm install` to install the dependencies
- run `npm run bootstrap` to initialize the database
- run `npm start` to run the API server
- now it is possible hit the urls:
  * http://localhost:3000/avgtempinsfax
  * http://localhost:3000/currenttempincovilha
- run `npm run test` for running the testscripts
- the application now works locally, the nex steps descibe how to get it working in AWS
- for the stack to work in AWS, create a Secret in the secrets manager via the console, called 'weatherAppSecrets'. Add a secret value with the key 'dbConnection' and the value of the connect string to the mongoDB like 'mongodb+srv://`<user>`:`<password>`@`<cluster>`.mongodb.net/retryWrites=true&w=majority'
- cd ../serverlesWeatherCDK
- add a .env file with this:
```
CDK_DEFAULT_ACCOUNT=<accountid>
CDK_DEFAULT_REGION=eu-central-1
AWS_PROFILE=default
SECRETSMANAGER_ARN=arn:aws:secretsmanager:eu-central-1:<accountid>:secret:weatherAppSecrets-<something>
```
- take care of a working AWS CLI environment, and have the aws config setup with your credentials 
- run `npm install`
- run `npx cdk diff` to see what will be changed
- run `npx cdk deploy` to deploy the stack to AWS
- check the Outputs section at the bottom where you can find the url for your API Gateway
- test it with given API paths.

## CDK remarks
Check the CDK stack code [here](./infra/lib/serverless_weather_cdk-stack.ts)
CDK creates a cloudformation template which can be found [here](./infra/cloudformationTemplate.json) (note that the accountid is removed from this template)

## Evidence
### Printscreens of mongoDB (see for all the documents [this file](app/init/init.json))
![](mongoDBOverview.png)
![](mongoDB_live_temp.png)
![](mongoDB_hist_temp.png)

### Result of the api's
![](currenttempincovilha.png)
![](avgtempinsfax.png)

### Result of the pipeline
In GitLab I created a build and test pipeline (see [this file])(.gitlab-ci.yml) for the configuration). The results of the pipeline can be found [here](https://gitlab.com/peter_bosch/myweatherapp/pipelines/138904266)

## Final Notes
- Normally I would setup the lambda in a VPC and connect the Lambda VPC with the Atlas VPC via a Peering Conneciton for extra security on the database. But as this is only available from M10 and upwards @MongoDB Atlas, and I am using a M0 free tier setup, I cannot establish this. 