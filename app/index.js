'use strict'
process.env.RUN_LOCAL = true;

const processRequest = require('./lib/processRequest').handler;
const express = require('express');

const app = express();
const port = 3000;

;(async () => {
  app.get('/currenttempincovilha', async (req,res) => res.send(await processRequest(req)));
  app.get('/avgtempinsfax', async (req,res) => res.send(await processRequest(req)));
  
  app.listen(port, () =>console.log(`api server listening at http://localhost:${port}`));
})();