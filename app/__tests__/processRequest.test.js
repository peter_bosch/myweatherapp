'use strict'

const api = require('../lib/processRequest.js')
process.env.RUN_LOCAL = true;

;(async () => {

  const eventTemplate = {
    httpMethod: 'GET',
    resource: '',
    path: '',
    pathParameters: {
    }
  }

  test('Test retrieving current temp OK', async () => {
    const event = {...eventTemplate}
    event.path = '/currenttempincovilha'
    const response = await api.handler(event)
    expect(response.statusCode).toBe(200)
  })

  test('Test retrieving average temp OK and equal to 23.5', async () => {
    const event = {...eventTemplate}
    event.path = '/avgtempinsfax'
    const response = await api.handler(event)
    expect(response.statusCode).toBe(200)
    const body = JSON.parse(response.body)
    expect(body.average).toBe(23.5)
  })

  test('Test unknown url', async () => {
    const event = {...eventTemplate}
    event.path = '/unknown'
    const response = await api.handler(event)
    expect(response.statusCode).toBe(400)
  })
})();

