'use strict'
const MongoClient = require('mongodb').MongoClient;
const dotenv = require('dotenv');
const AWS =require('aws-sdk');

//for performance reasons, setup the connection outside the handler so it can be re-used 
const dbName = 'weather';
var client, url, db;

const secretsManager = new AWS.SecretsManager({
  region: process.env.AWS_REGION
});

const createResponse = (code, body) => {
  if (process.env.RUN_LOCAL) {
    client.close();
    db = client = undefined;
  }
  return {
    statusCode: code,
    body: JSON.stringify(body)
  }
} 

exports.handler = async (event, context = undefined) => {
  console.log('API processRequest started');
  try {

    // do this only when we do not have a connection to the database
    if (typeof db === 'undefined') {
      //check if we run locally or in lambda:
      if (process.env.RUN_LOCAL) {
        console.log('Running locally, using the .env variable for connectionstring');
        dotenv.config();
        url = process.env.DB_CONNECTION;
      } else {
        console.log('Running in Lambda, using encypted secrets and decrypt for connectionstring');
        const data = await secretsManager.getSecretValue({
          SecretId: 'weatherAppSecrets',
        }).promise();
        if (data && data.SecretString) {
          url=JSON.parse(data.SecretString).dbConnection;
        } else throw 'Error fetching the secret';   
      }

      // Use connect method to connect to the Server
      client = await MongoClient.connect(url, {  useNewUrlParser: true, useUnifiedTopology: true });
      db = client.db(dbName);
    }
    
    if (!process.env.RUN_LOCAL) {
      // as we do not close the db connection, execution should not wait for the event loop to end
      context.callbackWaitsForEmptyEventLoop = false;
    }

    // check the path on what to do
    if (event.path === '/currenttempincovilha') {

      // get the live temps for the right city
      const filter = {
        'city': 'Covilha', 
        'country': 'Portugal'
      };
      
      // sort latest first
      const sort = {
        'date': -1
      };

      //fetch only the top one
      const limit = 1;

      const result = await db.collection('live_temp').find(filter, { sort: sort, limit: limit }).toArray();

      return createResponse(200, result[0]);
    } else if (event.path === '/avgtempinsfax') {
      // filter on the city and the dates
      const filter = {
        'city': 'Sfax', 
        'country': 'Tunisia',
        'date': {
          $gte: new Date('2020-06-01T00:00:00.000Z'),
          $lt: new Date('2020-07-01T00:00:00.000Z')
        }
      };
      const allTempsInJune = await db.collection('hist_temp').find(filter).toArray();
      let total = 0, 
          n = 0;

      // calculate the some and the number of elements 
      allTempsInJune.forEach(element => {
        n += 1;
        total += element.avg
      })

      let avg = total / n;
      console.log(`Average temp in Sfax Tunisia in June is ${avg}`);

      let result = {
        total: total,
        days: n,
        average: avg
      }  

      return createResponse(200, result);
    } else {
      let message = `"Error, unknown path ${event.path}"`;
      console.log (message);
      return createResponse(400, message);
    }
  } catch (error) {
    console.error (`Error occurred in processRequest: ${error.stack}`);
    return createResponse(400, error);
  }
}


