const MongoClient = require('mongodb').MongoClient;
const fs = require('fs');
require('dotenv').config();

(async function() {
  const url = process.env.DB_CONNECTION;
  const dbName = "weather";
  let client;

  try {
    // Use connect method to connect to the Server
    client = await MongoClient.connect(url, { useUnifiedTopology: true });

    const db = client.db(dbName);

    
    // read the init file
    const file = fs.readFileSync('./init/init.json', 'utf8');
    const data = JSON.parse(file);

    

    // go through the collections and add the data
    for (collection in data) {
      // create a date type for each date in the json file
      const processedData = data[collection].map(obj => {
        obj.date = new Date(obj.date);
        return obj;
      })
      // first delete all data
      await db.collection(collection).deleteMany({});
      //then add the data from the file
      await db.collection(collection).insertMany(processedData);
    }
    console.log ('database initialized');
  } catch (err) {
    console.log(err.stack);
  }

  if (client) {
    client.close();
  }
})();