import * as cdk from '@aws-cdk/core';
import * as apigateway from '@aws-cdk/aws-apigateway';
import * as lambda from '@aws-cdk/aws-lambda';
import * as secretsmanager from '@aws-cdk/aws-secretsmanager';

export class ServerlessWeatherCdkStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const handler = new lambda.Function(this, 'CurrentTempHandler', {
      runtime: lambda.Runtime.NODEJS_10_X, // So we can use async in widget.js
      code: lambda.Code.fromAsset('../app/lib'),
      handler: 'processRequest.handler',
      environment: {
      }
    });

    // fetch the secret from the secretsmanager
    const secman = secretsmanager.Secret.fromSecretArn(this, 'secretsmanager', process.env.SECRETSMANAGER_ARN!);
    secman.grantRead(handler);
    
    const api = new apigateway.RestApi(this,'weather-api', {
      restApiName: 'Weather Service',
      description: 'This service serves weather api\'s.'
    });

    const getLambdaIntegration = new apigateway.LambdaIntegration(handler, {
      requestTemplates: { 'application/json': '{ "statusCode": "200" }' }
    });

    api.root.addResource('currenttempincovilha').addMethod('GET', getLambdaIntegration);
    api.root.addResource('avgtempinsfax').addMethod('GET', getLambdaIntegration);
  }
}
